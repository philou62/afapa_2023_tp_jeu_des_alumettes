package jeuAlumettes;

public class Jeu {

	int nbreAlumettes = 17;

	public int getNbreAlumettes() {
		return nbreAlumettes;
	}

	public void setNbreAlumettes(int nbreAlumettes) {
		this.nbreAlumettes = nbreAlumettes;
	}

	public void afficherAlumettes() {
		for (int i = 0; i < nbreAlumettes; i++) {
			System.out.print("| ");
		}
	}

	public void commencerJeu() {
		afficherAlumettes();
		Jeu jeu = new Jeu();
		int millis = 3000;

		while (nbreAlumettes > 1) {
			try {
				Joueur joueur = new Joueur();
				int choix = joueur.propositionJoueur();
				nbreAlumettes = (nbreAlumettes - choix);

				System.out.println("Il reste " + nbreAlumettes + " alumettes");
				afficherAlumettes();
				if (nbreAlumettes == 1) {
					System.out.println("\nL'ordinateur a perdu");
				} else {
					System.out.println("");
					Thread.sleep(millis);
					Ordinateur lui = new Ordinateur();
					int choixOrdi = lui.propositionOrdinateur();
					nbreAlumettes = nbreAlumettes - choixOrdi;
					System.out.println("Il reste " + nbreAlumettes + " alumettes");
					afficherAlumettes();
					System.out.println("\nA toi de jouer");
					if (nbreAlumettes == 1) {
						System.out.println("\nTu as perdu");
					}
					Thread.sleep(millis);
				}
			} catch (Exception e) {
				System.out.println("erreur");
			}

		}
		System.out.println("fin du jeu");

	}

	public void menuJeu() {

	}
}
