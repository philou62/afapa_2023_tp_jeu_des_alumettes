package jeuAlumettes;

import java.util.Scanner;

public class Joueur {

	public Joueur() {
		
	}


	public int propositionJoueur() {
		Scanner sc = new Scanner(System.in);
		
		int nbAlumettes = 0;

		System.out.println("\nChoisir le nombre d'alumette a prendre, une minimum , trois maximum");
		nbAlumettes = sc.nextInt();

		while (nbAlumettes < 1 || nbAlumettes > 3) {
			System.out.println("Le nombre d'alumette prise est interdit!, Choisir un chiffre entre 1 et 3");
			nbAlumettes = sc.nextInt();
		}

		System.out.println("*** Tu as pris " + nbAlumettes + " alumettes ***");

		return nbAlumettes;

	}
}
